<?php

namespace App\Controller;

use App\Service\BackupStructureService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/v1/backupstructure", name="Backup")
 */
class BackupStructureController extends AbstractFOSRestController
{
    private $backupStructureService;

    public function __construct(BackupStructureService $backupStructureService)
    {
        $this->backupStructureService = $backupStructureService;
    }

    /**
     * BackupLogic.
     *
     * @Rest\Post("/", name="index")
     */
    public function index(Request $request): View
    {
        $inputData = json_decode($request->getContent(), true);

        $result = $this->backupStructureService->metaStructureBackup($inputData);
        if ('error' === $result['status']) {
            return View::create([$result['message']], Response::HTTP_BAD_REQUEST);
        }

        if ('success' === $result['status'] && 'Structure created sucessfully' === $result['message']) {
            return View::create($result, Response::HTTP_CREATED);
        }

        return View::create($result, Response::HTTP_OK);
    }
}
