<?php

namespace App\Controller;

use App\Service\BackupDataService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/v1/backupdata", name="BackupData")
 */
class BackupDataController extends AbstractFOSRestController
{
    private $backupDataService;

    public function __construct(BackupDataService $backupDataService)
    {
        $this->backupDataService = $backupDataService;
    }

    /**
     * BackupLogic.
     *
     * @Rest\Post("/", name="index")
     */
    public function index(Request $request): View
    {
        $inputData = json_decode($request->getContent(), true);
        $result = $this->backupDataService->metaDataBackup($inputData);

        if ('error' === $result['status']) {
            return View::create([$result['message']], Response::HTTP_BAD_REQUEST);
        }

        return View::create($result, Response::HTTP_OK);
    }
}
