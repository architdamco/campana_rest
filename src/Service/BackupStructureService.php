<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class BackupStructureService
{
    private $entityManagerInterface;

    public function __construct(EntityManagerInterface $entityManagerInterface)
    {
        $this->entityManagerInterface = $entityManagerInterface;
        $this->dataTypeArray = [
            'ID' => 'VARCHAR', 'REFERENCE' => 'VARCHAR', 'STRING' => 'VARCHAR',
            'FLOAT' => 'DECIMAL', 'BOOLEAN' => 'BOOLEAN', 'PICKLIST' => 'VARCHAR',
            'DATE' => 'DATE', 'DATETIME' => 'TIMESTAMPTZ', 'TEXTAREA' => 'TEXT',
            'DOUBLE' => 'NUMERIC',  'PHONE' => 'VARCHAR', 'ADDRESS' => 'TEXT',
            'EMAIL' => 'VARCHAR', 'URL' => 'VARCHAR',
            ];
    }

    public function metaStructureBackup(array $data)
    {
        $result = [];
        try {
            if (false === $this->validateSchema($data['Object_API_Name'])) {
                $stmt = $this->buildSchemaStatement($data);
                $this->createSchema($stmt);
                $this->buildComment($data);
                $result['status'] = 'success';
                $result['message'] = 'Structure created sucessfully';
            } else {
                $alterQryStmt = $this->buildAlterStatement($data);
                if ($alterQryStmt != '') {
                    $this->createSchema($alterQryStmt);
                }
                $result['status'] = 'success';
                $result['message'] = 'Structure created sucessfully';
            }
        } catch (\Exception $e) {
            $result['status'] = 'error';
            $result['message'] = $e->getMessage();
        }

        return $result;
    }

    public function validateSchema(string $entity)
    {
        $entities = [];
        $em = $this->entityManagerInterface;
        $meta = $em->getConnection()->getSchemaManager()->listTables();

        foreach ($meta as $m) {
            $entities[] = $m->getName();
        }

        if (in_array(strtolower($entity), $entities)) {
            return true;
        }

        return false;
    }

    public function buildAlterStatement(array $data)
    {
        $alterColumnStatement = '';
        $alterQueryStatement = 'ALTER TABLE '.strtolower($data['Object_API_Name']).' ';

        foreach ($data['metadata'] as $row) {
            if ('True' === $row['IsActive']) {
                $result = $this->getAllColumn();

                if (!array_key_exists(strtolower($row['Field_API_Name']), $result[strtolower($data['Object_API_Name'])])) {
                    $constraint = '';
                    $dataType = $this->dataTypeArray[$row['Data_Type']];

                    if ('ID' == $row['Field_API_Name']) {
                        $constraint = 'PRIMARY KEY NOT NULL';
                    } else {
                        if ('True' === $row['IsRequired']) {
                            $constraint = 'NOT NULL';
                        }
                    }

                    if ('BOOLEAN' === $dataType || 'JSON' === $dataType || 'TEXT' === $dataType) {
                        $alterColumnStatement .= 'ADD COLUMN '.$row['Field_API_Name'].' '.$dataType.',';
                    } elseif ('DATE' === $dataType || 'TIMESTAMPTZ' === $dataType) {
                        $alterColumnStatement .= 'ADD COLUMN '.$row['Field_API_Name'].' '.$dataType.' '.$constraint.',';
                    } else {
                        $alterColumnStatement .= 'ADD COLUMN '.$row['Field_API_Name'].' '.$dataType.'('.$row['Length'].') '.$constraint.',';
                    }
                }
            }
        }

        if ($alterColumnStatement != '') {
            $alterQueryStatement .= substr($alterColumnStatement, 0, -1);
        } else {
            $alterQueryStatement = $alterColumnStatement;
        }

        return $alterQueryStatement;
    }

    public function buildSchemaStatement(array $data)
    {
        $stmt = 'CREATE TABLE '.strtolower($data['Object_API_Name']);
        $dataStmt = '(';

        foreach ($data['metadata'] as $row) {
            $constraint = '';

            if (array_key_exists($row['Data_Type'], $this->dataTypeArray)) {
                $dataType = $this->dataTypeArray[$row['Data_Type']];
            } else {
                $dataType = 'VARCHAR';
                $row['Length'] = '255';
            }

            if ('False' !== $row['IsActive']) {
                if ('Id' == $row['Field_API_Name']) {
                    $constraint = 'PRIMARY KEY NOT NULL';
                } else {
                    if ('True' === $row['IsRequired']) {
                        $constraint = 'NOT NULL';
                    }
                }

                if ('BOOLEAN' === $dataType || 'JSON' === $dataType || 'TEXT' === $dataType) {
                    $dataStmt .= $row['Field_API_Name'].' '.$dataType.',';
                } elseif ('DATE' === $dataType || 'TIMESTAMPTZ' === $dataType) {
                    $dataStmt .= $row['Field_API_Name'].' '.$dataType.' '.$constraint.',';
                } else {
                    if (trim($row['Length']) == '') {
                        $row['Length'] = 255;
                    }
                    $dataStmt .= $row['Field_API_Name'].' '.$dataType.'('.$row['Length'].') '.$constraint.',';
                }
            }
        }
        $dataStmt = substr($dataStmt, 0, -1);
        $dataStmt .= ')';
        $stmt .= $dataStmt;

        return $stmt;
    }

    public function buildComment(array $data)
    {
        $stmtTable = 'comment on table '.strtolower($data['Object_API_Name'])." is '".strtolower($data['Object_Name'])."'";
        $this->createSchema($stmtTable);
        foreach ($data['metadata'] as $row) {
            $field_Label = str_replace("'", '', $row['Field_Label']);
            $stmtColumn = 'comment on column '.strtolower($data['Object_API_Name']).'.'.strtolower($row['Field_API_Name'])." is '".$field_Label."'";
            $this->createSchema($stmtColumn);
        }
    }

    public function getAllColumn()
    {
        $entities = [];
        $em = $this->entityManagerInterface;
        $meta = $em->getConnection()->getSchemaManager()->listTables();

        foreach ($meta as $m) {
            $entities[$m->getName()] = $m->getColumns();
        }

        return  $entities;
    }

    public function createSchema(string $sql)
    {
        $conn = $this->entityManagerInterface->getConnection();

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt;
    }
}
