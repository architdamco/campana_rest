<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class BackupDataService
{
    private $entityManagerInterface;

    public function __construct(EntityManagerInterface $entityManagerInterface)
    {
        $this->entityManagerInterface = $entityManagerInterface;
    }

    public function metaDataBackup(array $data)
    {
        $result = [];
        try {
            $this->buildInsertStatement($data);
            $result['status'] = 'success';
            $result['message'] = 'Data updated sucessfully';
        } catch (\Exception $e) {
            $result['status'] = 'error';
            $result['message'] = $e->getMessage();
        }

        return $result;
    }

    //To do insert data
    public function buildInsertStatement(array $data)
    {
        $stmt = 'INSERT INTO '.strtolower($data['Object_API_Name']);

        $newStmt = '';

        foreach ($data['data'] as $key => $val) {
            $insertValues = '';
            unset($val['attributes']);
            $dataAttribute = '';

            foreach ($val  as $dataKey => $dataValues) {
                $dataAttribute .= strtolower($dataKey).',';

                if (!is_array($dataValues)) {
                    if (is_bool($dataValues)) {
                        $dataValues = (int) $dataValues;
                    } else {
                        $dataValues = str_replace("'", ' ', $dataValues);
                    }
                    $insertValues .= "'".$dataValues."',";
                } else {
                    $insertValues .= "'".serialize($dataValues)."',";
                }
            }

            $insertKey = '('.substr($dataAttribute, 0, -1).')';
            $insertValues = '('.substr($insertValues, 0, -1).')';
            $newStmt = $stmt.$insertKey.' values '.$insertValues;
            $onConflictStmt = $this->buildUpdateInsertStatement($newStmt);
            $updateSql = $this->buildUpdatStatement($val, $data['Object_API_Name']);
            $sql = $onConflictStmt.$updateSql;

            $this->createSchema($sql);
        }

        $stmt = $sql;

        return  $stmt;
    }

    public function buildUpdateInsertStatement(string $insertStmt)
    {
        $onConflictStmt = ' ON CONFLICT (id) DO';

        $insertStmt .= $onConflictStmt;

        return $insertStmt;
    }

    public function buildUpdatStatement(array $data, string $tableName)
    {
        $cols = [];

        foreach ($data as $key => $val) {
            $key = trim(strtolower($key));
            if ('id' === $key) {
            } else {
                $cols[] = "$key = EXCLUDED".'.'.$key;
            }
        }

        $updateSql = ' UPDATE SET '.implode(', ', $cols);

        return $updateSql;
    }

    public function createSchema(string $sql)
    {
        $conn = $this->entityManagerInterface->getConnection();

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt;
    }
}
